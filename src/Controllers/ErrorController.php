<?php
namespace Portfolio\Controllers;

class ErrorController extends Controller {

    public function __construct($router) {
        parent::__construct($router);
    }

    public function index() {
        $this->render('pages/404.twig', []);
    }
}