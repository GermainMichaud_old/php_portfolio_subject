<?php
namespace Portfolio\Controllers;

use Portfolio\Models\ProjetModel;

class ProjetController extends Controller
{
    private $db_table = "PORTFOLIO";
    private $projectModel;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->projectModel = new ProjetModel();
    }

    public function getAll()
    {
        $stmt = $this->projectModel->getAll($this->db_table);
        $this->render('pages/projets.twig', ['data' => $stmt]);
    }

    public function getOne($param)
    {
        $id = $param['id'];
        $stmt = $this->projectModel->getOne($this->db_table, $id);
        $this->render('pages/projets.twig', ['data' => $stmt]);
    }
}
