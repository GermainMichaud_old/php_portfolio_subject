<?php
namespace Portfolio\Controllers;

use Portfolio\Models\UserModel;

class PresentationController extends Controller {
    private $db_table = "USER";
    public $userModel;

    public function __construct($router) {
        parent::__construct($router);
        $this->userModel = new UserModel();
    }

    public function getData() {
        $stmt = $this->userModel->get($this->db_table);
        $this->render('pages/presentation.twig', ['data' => $stmt]);
    }
}