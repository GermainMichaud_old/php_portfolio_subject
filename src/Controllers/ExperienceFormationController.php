<?php
namespace Portfolio\Controllers;

use Portfolio\Models\ExperienceModel;
use Portfolio\Models\FormationModel;

class ExperienceFormationController extends Controller
{
    private $db_table_exp = "EXPERIENCES";
    private $db_table_form = "FORMATIONS";
    private $expModel;
    private $formModel;

    public function __construct($router)
    {
        parent::__construct($router);
        $this->expModel = new ExperienceModel();
        $this->formModel = new FormationModel();
    }

    public function getAll()
    {
        $experiences = $this->getExperiences(false);
        $formations = $this->getFormations(false);
        $this->render('pages/experiences.twig', ['experiences' => $experiences, 'formations' => $formations]);
    }

    public function getExperiences($render = true) {
        $experiences = $this->expModel->getAll($this->db_table_exp);
        if ($render) {
            $this->render('pages/experiences.twig', ['experiences' => $experiences]);
        } else {
            return $experiences;
        }
    }

    public function getFormations($render = true) {
        $formations = $this->formModel->getAll($this->db_table_form);
        if ($render) {
            $this->render('pages/experiences.twig', ['formations' => $formations]);
        } else {
            return $formations;
        }
    }
}
