<?php
namespace Portfolio\Controllers;
use PhpParser\Node\Expr\Array_;

class Controller {

    private $twig;

    private $router;

    public function __construct($router) {
        $this->router = $router;
        try {
            $loader = new \Twig\Loader\FilesystemLoader('../src/views');
            $this->twig = new \Twig\Environment($loader);
        } catch (Exception $e) {
            die('ERROR => ' . $e->getMessage());
        }
    }


    public function render($path, $data = []) {
        echo $this->twig->render($path, $data);
    }
}