<?php

namespace Portfolio\Models;
use Portfolio\Models;

class FormationModel extends Model {

    /**
     * @param $table
     * @param null $filter
     * @return array
     */
    public function getAll($table, $filter = null): array {
        $query = "SELECT * FROM " . $table . ($filter ? " " . $filter : "");
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll();
    }

}