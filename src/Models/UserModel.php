<?php

namespace Portfolio\Models;
use Portfolio\Models;

class UserModel extends Model {

    public function get($table, $filter = null) {
        $stmt = $this->db->prepare("SELECT * FROM " . $table);
        $stmt->execute();
        return $stmt->fetch();
    }

}