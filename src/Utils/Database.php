<?php
namespace Portfolio\Utils;

use PDO;

class Database
{
    private $db_host = 'localhost:3306';
    private $db_name = 'PHP_PORTFOLIO_PROJECT';
    private $db_user = "root";
    private $db_pass = "password";

    public $db;

    public function __construct() {
        try {
            $database = new PDO("mysql:host=" . $this->db_host . ";dbname=" . $this->db_name, $this->db_user, $this->db_pass, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);
            $this->db = $database;
        } catch (\PDOException $e) {
            var_dump($e);
            die();
        }
    }
}