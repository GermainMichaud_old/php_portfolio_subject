<?php
require_once '../vendor/autoload.php';
require '../src/Utils/Router.php';

$router = new Router();
$router->setBasePath('');


$router->map('GET', '/', [
    'method' => 'getData',
    'controller' => 'PresentationController'
], 'presentation');
$router->map('GET', '/experiences', [
    'method' => 'getAll',
    'controller' => 'ExperienceFormationController'
], 'experiences-formations');
$router->map('GET', '/projets', [
    'method' => 'getAll',
    'controller' => 'ProjetController'
], 'projets');
$router->map('GET', '/404', [
    'method' => 'index',
    'controller' => 'ErrorController'
], 'error404');

$match = $router->match();

if(!$match) {
    header('Location: /404');
}

$methodToUse = $match['target']['method'];
$controllerToUse = 'Portfolio\\Controllers\\' . $match['target']['controller'];

$controller = new $controllerToUse($router);
$controller->$methodToUse($match['params']);









/*
$uri = rtrim(dirname($_SERVER["SCRIPT_NAME"]), '/');
$uri = '/' . trim(str_replace($uri, '', $_SERVER["REQUEST_URI"]), '/');
$uri = urldecode($uri);


if ($uri === '/') {
    require_once '../src/Controllers/PresentationController.php';
    $presentation = new PresentationController();
    $presentation->getData();

} else if ($uri === '/projets') {
    require_once '../src/Controllers/ProjetController.php';
    $itemsProjects = new ProjetController();
    $itemsProjects->getAll();

} else if($uri === '/experiences') {
    require_once '../src/Controllers/ExperienceFormationController.php';
    $expAndForm = new ExperienceFormationController();
    $expAndForm->getAll();
} else {
    require_once '../src/Controllers/ErrorController.php';
    return new ErrorController();
}
*/